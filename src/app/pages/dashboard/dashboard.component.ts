import { Component, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CommonService } from '../../service/common.service'

export interface Company {
  name: string;
  _id: string;
}
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(public commonservice: CommonService) { }
  resData;
  sectionHead;
  companyNames = [];
  section: boolean = false;
  myControl = new FormControl();
  filteredOptions: Observable<Company[]>;

  ngOnInit() {
    this.getOptons();
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.companyNames.slice())
      );
  }

  displayFn(user: Company): string {
    return user && user.name ? user.name : '';
  }

  private _filter(name: string): Company[] {
    const filterValue = name.toLowerCase();
    return this.companyNames.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }


  getOptons() {
    this.commonservice.getOptons().subscribe((res) => {
      this.resData = res;
      this.companyNames = this.resData.data;
    })

  }
  submitSearch() {
    this.sectionHead = this.myControl.value.name
    this.section = true
  }
}



