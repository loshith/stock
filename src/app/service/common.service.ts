import { Injectable } from '@angular/core';
import { Observable, of, throwError } from "rxjs";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  url = environment.backendUrl;
  constructor(public http:HttpClient) { }

  getOptons(){
    let data={
    }
    return this.http.post(`${this.url}/common/getCompany`,data);
  }
}
